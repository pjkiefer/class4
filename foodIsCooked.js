function foodIsCooked(kind, internalTemp, doneness){
    if(kind === 'chicken' && internalTemp >= 165)
        return true;
    else if(kind === 'beef')
    {
        if(doneness === 'rare' && internalTemp >= 125)
            return true;
        else if(doneness === 'medium' && internalTemp >= 135)
            return true;
        else if(doneness === 'well' && internalTemp >= 155)
            return true;
    }
    
    return false;
        
};
