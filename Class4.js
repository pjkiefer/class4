function getDeck() {
    let deck = [];
    const cardName = ['Ace', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King'];
    const suit = ['hearts', 'spades', 'clubs', 'diamonds'];
    const cardValue = [11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10];

    for (let i = 0; i < suit.length; i++) {
        for (let j = 0; j < cardName.length; j++) {
            deck.push({
                val: cardValue[j],
                displayVal: cardName[j],
                suit: suit[i]
            });
        }
    }
    return deck;
};


let deck;
let deckLength = 52;
let player;
let dealer;

function removeCard(index){
    deck.splice(index,1);
    deckLength--;
}

function dealDeck(){
    if(!deck)
        deck = getDeck();
};

function InitNewGame(){
    let name = prompt("Please enter a player name","");
    if(!name)
        name = 'unknown';
    dealer = new CreatePlayer('Dealer');
    dealer.setAsDealer();
    player = new CreatePlayer(name);
    dealDeck();
    deckLength = 52;
    while(dealer.checkDealerHold() === false)
    {
        dealer.drawCard();
    }
    let x = document.getElementById('result');
    x.innerHTML = "";
    addCardToTable(dealer.showCards(),'dealer', false);
};
function CreatePlayer(name){
    let playerName = name;
    let playerHand = [];
    
    let isDealer = false;
    let dealerHold = false;
    let hasAce = false;
    this.setPlayerName = function(name){
        playerName = name;
    };
    this.getPlayerName = function(){
        return playerName;
    };
    this.setAsDealer = function(){
        isDealer = true;
    }
    this.drawCard = function(){
        
        if(isDealer === true && hasAce === true && this.getPoints().Points >= 18){
            dealerHold = true;
            return;
        }
        else if(isDealer === true && hasAce === false && this.getPoints().Points >= 17){
            dealerHold = true;
            return;
        }
       
        let cardLocation = Math.floor((Math.random()*deck.length));
        let card = deck[cardLocation];
        let newCard = {
            val:card.val,
            displayVal:card.displayVal,
            suit:card.suit
        };
        playerHand.push(newCard);
        removeCard(cardLocation);
        
        this.getPoints() >= 17 ? drawNewCard = false : drawNewCard = true;
    }
    this.showCards = function(){
        return playerHand;
    }
    this.checkDealerHold = function(){
        return dealerHold;
    }
    this.getPoints = function(){
        let aceCount = 0;
        let totalPoints = 0;
        for(let i =0;i < playerHand.length;i++)
        {
            if(playerHand[i].displayVal === 'Ace')
            {
                aceCount++;
            }
            else
            {
                totalPoints += playerHand[i].val;
            }
        }
        if(aceCount === 1 && totalPoints <= 10)
            {
                totalPoints += 11;
                hasAce = true;
            }
            else if(aceCount === 1 && totalPoints >= 11)
            {
                totalPoints += 1;
                hasAce = false;
            }
            else if(aceCount === 2 && totalPoints <= 9)
            {
                totalPoints += 12;
                hasAce = true;
            }
            else if(aceCount === 2 && totalPoints >= 10)
            {
                totalPoints += 2;
                hasAce = false;
            }
            else if(aceCount === 3 && totalPoints <= 8)
            {
                totalPoints += 13;
                hasAce = true;
            }
            else if(aceCount === 3 && totalPoints >= 9)
            {
                totalPoints += 3;
                hasAce = false;
            }
            else if(aceCount === 4 && totalPoints <= 7)
            {
                totalPoints += 14;
                hasAce = true;
            }
            else if(aceCount === 3 && totalPoints >= 8)
            {
                totalPoints += 4;
                hasAce = false;
            }
            return {
                Points:totalPoints,
                HasAce: hasAce === true ? 'yes' : 'no'
            }
    }
};

function playerAddCard(){
    player.drawCard();
    addCardToTable(player.showCards(),'player', true);

};
function checkForWinner(){
    const playerPoints = player.getPoints().Points;
    const dealerPoints = dealer.getPoints().Points;
    let x = document.getElementById('result');
    if(playerPoints > dealerPoints && playerPoints < 21){
        x.innerHTML = `${player.getPlayerName()} has won with ${playerPoints}\nwhile the dealer has ${dealerPoints}`;
    }else if(playerPoints === 21 && dealerPoints != 21){
        x.innerHTML = `${player.getPlayerName()} BlackJack! ${playerPoints}\n the dealer has ${dealerPoints}`;
    }
    else if(playerPoints > 21 && dealerPoints <= 21){
        x.innerHTML = `${player.getPlayerName()} sorry you're over with ${playerPoints}\nThe dealer has ${dealerPoints}`;
    }
    else if(dealerPoints >= playerPoints && dealerPoints <= 21){
        x.innerHTML = `${player.getPlayerName()} loses with ${playerPoints}\nThe dealer has ${dealerPoints}`;
    }
    
};

window.onload = function(){
    InitNewGame();
};

function addCardToTable(hand, playerType, showCard){
    const myTable = document.getElementById('blackJackTable');
    let rowCount = myTable.rows.length;
    while(rowCount < hand.length + 1){
        let row = myTable.insertRow(-1);
        let cell1 = row.insertCell(0);
        cell1.innerHTML = '&nbsp;';
        let cell2 = row.insertCell(1);
        cell2.innerHTML = '&nbsp;';
        rowCount = myTable.rows.length;
    }
    if(playerType === 'dealer'){
        for(let i = 1; i < rowCount;i++){
            if(showCard === false && i === 1){
                myTable.rows[i].cells[0].innerHTML =`${hand[i - 1].displayVal} of ${hand[i - 1].suit}`;
            }
            else{
                myTable.rows[i].cells[0].innerHTML = 'CARD';
            }
        }
    }
    if(playerType === 'player'){
        for(let i = 1; i < rowCount;i++){
            myTable.rows[i].cells[1].innerHTML =`${hand[i - 1].displayVal} of ${hand[i - 1].suit}`;
        }
    }
}


